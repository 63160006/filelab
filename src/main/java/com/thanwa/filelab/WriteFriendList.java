/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.filelab;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tud08
 */
public class WriteFriendList {
    public static void main(String[] args) {
        ArrayList<Friend> friendList = new ArrayList<>();
        friendList.add(new Friend("thanwa", 20,"0909090909"));
        friendList.add(new Friend("arthit", 20,"0901234567"));
        friendList.add(new Friend("thuch", 18,"0202020202"));
        
        FileOutputStream fos = null;
        try {
            //friends.dat
            File file = new File("list_of_friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friendList);
            oos.close();
            fos.close();
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
