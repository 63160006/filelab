/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.filelab;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tud08
 */
public class WriteFriend {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friend friend1 = new Friend("thanwa", 20,"0909090909");
            Friend friend2 = new Friend("arthit", 20,"0901234567");
            Friend friend3 = new Friend("thuch", 18,"0202020202");
            //friends.dat
            File file = new File("friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(friend1);
            oos.writeObject(friend2);
            oos.writeObject(friend3);
            oos.close();
            fos.close();
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
